<?php

    require (__DIR__."/vendor/autoload.php");
    require (__DIR__."/Import.class.php");
    require (__DIR__."/Report.class.php");

    if(isset($argv[1])) {
        $R = new Report;
        $R->generateReport($argv[1]);
    }
