<?php

class Report {

    private $group_name_col = 3;
    private $header = true;
    private $col_names = array (
        '0' => array ( 'name' => 'courier', 'col' => 0),
        '1' => array ( 'name' => 'waybill_number', 'col' => 1),
        '2' => array ( 'name' => 'order_number', 'col' => 4),
        '3' => array ( 'name' => 'amount', 'col' => 2, 'sum' => true)
    );

    public function groupArray($data) {

        $groups = array();

        foreach ($data as $keyRow => $row) {

            if ($this->header == true && $keyRow == 0)
                continue;

            foreach ($this->col_names as $k => $v) {

                if (!isset($groups[$row[$this->group_name_col]])) {
                    $groups[$row[$this->group_name_col]] = array('data' => array(), 'sum' => 0);
                }

                $groups[$row[$this->group_name_col]]['data'][$keyRow][$v['name']] = $row[$v['col']];

                if (isset($v['sum']) && $v['sum'] == true) {
                    $groups[$row[$this->group_name_col]]['sum'] += $row[$v['col']];
                }

            }
        }

        return $groups;
    }

    public function generateFile($name, $data) {

        $name = $name."_report_".date("YmdHis").".json";
        $data = json_encode($data);

        file_put_contents(__DIR__."/".$name, $data);

    }

    public function generateReport($file) {

        $data = Import::getData($file);

        if(isset($data) && !empty($data))
            $group = $this->groupArray($data);

        if(isset($group) && !empty($group)) {

            foreach ($group as $name => $data) {
                $this->generateFile($name, $data);
            }

        }
    }
}