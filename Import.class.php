<?php
    class Import {

        private $ext;

        public function setExt($file) {
            $this->ext = pathinfo($file, PATHINFO_EXTENSION);
        }

        public static function getData($file)
        {
            $import = new self();

            $import->setExt($file);

            switch ($import->ext) {
                case "xlsx":
                    if ($xlsx = SimpleXLSX::parse($file))
                        return $xlsx->rows();
                    else
                        return array();
                    break;

                default:
                    return array();
                    break;
            }
        }

    }